app_dir=$1
sudo apt-get update
sudo apt-get install nginx
sudo apt-get install mysql-server
sudo apt-get install php7.0 php7.0-mbstring php7.0-zip php7.0-xml php7.0-mysql php7.0-gd php7.0-sqlite
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
cd /var/lib && sudo git clone https://bitbucket.org/mashpy/servereditor.git
sudo apt-get install zip php7.0-curl
