username=$1
password=$2
directory=$3
apt-get install -y vsftpd libpam-pwdfile apache2-utils
cp config/vsftpd.conf /etc/vsftpd.conf
mkdir -p /etc/vsftpd
htpasswd -bd /etc/vsftpd/ftpd.passwd $username $password
cp config/vsftpd /etc/pam.d/vsftpd
useradd --home /home/vsftpd --gid nogroup -m --shell /bin/false vsftpd
mkdir -p /etc/vsftpd_user_conf
echo "local_root=$directory" | sudo tee /etc/vsftpd_user_conf/$username
mkdir -p $directory
chown www-data:www-data $directory
chmod 775 $directory
usermod -a -G www-data vsftpd
service vsftpd restart