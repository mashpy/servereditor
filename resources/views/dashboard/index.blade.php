@extends('layouts.master')

@section('content')

<div class="wrapper">
    @include('template.header')
    <!-- Left side column. contains the logo and sidebar -->
    @include('template.main_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                DashBoard
                <small>Control your server</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('root_url') }}"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('domain.index') }}">
                        <div class="info-box bg-light-blue">
                        <span class="info-box-icon"><i class="fa fa-krw"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">Domain</span>
                            <span class="info-box-text">Add Domain</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                          <span class="progress-description">
                            0 Domain added
                          </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('subdomain.index') }}">
                        <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-opera"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">Sub Domain</span>
                            <span class="info-box-text">Add Subdomain</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                  <span class="progress-description">
                    0 subdomain added.
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('mysql_users.index') }}">
                        <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-database"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">MYSQL Setup</span>
                            <span class="info-box-text">Add Database</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                  <span class="progress-description">
                    0 database account
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('filemanager.index') }}">
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-file-text"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number">File Manager</span>
                            <span class="info-box-text">Add,Edit,Delete files</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                  <span class="progress-description">
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('diskusages.index') }}">
                        <div class="info-box bg-light-blue">
                            <span class="info-box-icon"><i class="fa fa-globe"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number">Disk Usages</span>
                                <span class="info-box-text">Check your disk status</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                              <span class="progress-description">
                              </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="">
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-at"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number">Email Manager</span>
                                <span class="info-box-text">Create new mail account</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                              <span class="progress-description">
                                  0 account created.
                              </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('cronjobs.index') }}">
                        <div class="info-box bg-light-blue">
                            <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number">Cron Job</span>
                                <span class="info-box-text">Run Your scheduler</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                              <span class="progress-description">
                                  0 cronjob created.
                              </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('ftp.index') }}">
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-cubes"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number">FTP Account</span>
                                <span class="info-box-text">Add new ftp account</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                              <span class="progress-description">
                                  0 account created.
                              </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ route('manageusers.index') }}">
                        <div class="info-box bg-red">
                            <span class="info-box-icon"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number">User Account</span>
                                <span class="info-box-text">Add new user account</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                              <span class="progress-description">
                                  0 account created.
                              </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </a>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('template.footer')

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript::;">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript::;">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                <span class="pull-right-container">
                  <span class="label label-danger pull-right">70%</span>
                </span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@endsection

@section('extra_js_files')

@endsection

@section('extra_js_run')


@endsection
