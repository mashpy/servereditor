<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('/bower_components/AdminLTE/dist/img/avatar-blank.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Mahmudul Islam</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Key Features</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ route('root_url') }}"><i class="fa fa-home"></i> <span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-krw"></i> <span>Domain</span></a></li>
            <li><a href="#"><i class="fa fa-opera"></i> <span>Sub Domain</span></a></li>
            <li><a href="#"><i class="fa fa-database"></i> <span>MYSQL Setup</span></a></li>
            <li><a href="#"><i class="fa fa-file-text"></i> <span>File Manager</span></a></li>
            <li><a href="#"><i class="fa fa-globe"></i> <span>Advance DNS</span></a></li>
            <li><a href="#"><i class="fa fa-at"></i> <span>Email Manager</span></a></li>
            <li><a href="#"><i class="fa fa-clock-o"></i> <span>Cron Job</span></a></li>
            <li><a href="{{ route('ftp.index') }}"><i class="fa fa-cubes"></i> <span>FTP Account</span></a></li>
            <li><a href="#"><i class="fa fa-user"></i> <span>User Account</span></a></li>
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-user"></i> <span>Multilevel</span>--}}
                     {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="#">Link in level 2</a></li>--}}
                    {{--<li><a href="#">Link in level 2</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
