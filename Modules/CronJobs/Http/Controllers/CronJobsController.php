<?php

namespace Modules\CronJobs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CronJobs\Models\CronjobList;

class CronJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cron_lists = CronjobList::orderBy('created_at', 'desc')->get();
        return view('cronjobs::index')->with(compact('cron_lists'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('cronjobs::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('cronjobs::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('cronjobs::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }


    public function cronCreateshellHit(Request $request){
        $user_name = $request->get('user_name');
        $time = $request->get('time');
        $cron_script = $request->get('cron_script');


        $current_crontab = exec("su -l  $user_name -c 'crontab -l'", $crontab_log, $return_var);

        $current_crontab_text = implode("\n", $crontab_log);

        $execute_array = [
            ["echo \"$current_crontab_text\" > /home/$user_name/mycron", true],
            ["echo \"$time\" \"$cron_script\" >> /home/$user_name/mycron", true],
            ["su -l  $user_name -c 'crontab mycron'", true],
            ["rm /home/$user_name/mycron", true],
        ];

        $add_cron_shell = $this->execute_multiple($execute_array);



        if($add_cron_shell['result']){
            $cron_list = new \Modules\CronJobs\Models\CronjobList;
            $cron_list->user_name = $user_name;
            $cron_list->time = $time;
            $cron_list->cron_script = $cron_script;
            $cron_list->user_id = 1;
            $cron_list_save = $cron_list->save();
            if($cron_list_save){
                $json_output['result'] = true;
                $json_output['output'] = $cron_list;
            } else {
                $json_output['result'] = false;
            }
        } else {
            $json_output = $add_cron_shell;
        }

        return $json_output;
    }

    private function  execute_multiple($commands){
        $output = [];
        $output['result'] = true;
        $output['all_log'] = [];
        foreach($commands as $command){
            exec("sudo $command[0] 2>&1", $log, $return_var);
            $output['all_log'] = $log;
            if( $command[1] && $return_var !== 0){
                $output['result'] = false;
                break;
            }
        }

        return $output;
    }
}
