<?php

Route::group(['middleware' => 'web', 'prefix' => 'cronjobs', 'namespace' => 'Modules\CronJobs\Http\Controllers'], function()
{
    Route::any('/', [
        'as'   => 'cronjobs.index',
        'uses' => 'CronJobsController@index'
    ]);

    Route::any('create-cron-job', [
        'as'   => 'cron.create_account',
        'uses' => 'CronJobsController@cronCreateshellHit'
    ]);
});
