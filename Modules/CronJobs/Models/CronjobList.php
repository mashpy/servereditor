<?php

namespace Modules\CronJobs\Models;

use Illuminate\Database\Eloquent\Model;

class CronjobList extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}