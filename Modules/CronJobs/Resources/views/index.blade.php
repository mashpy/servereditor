@extends('layouts.master')

@section('content')

    <div class="wrapper">
    @include('template.header')
    <!-- Left side column. contains the logo and sidebar -->
    @include('template.main_sidebar')
    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Cron Jobs
                    <small>Add your cron script</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                    <li class="active">Here</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Add Your Cron script</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">User Name</label>
                                        <input class="form-control" required="required" id="user_name_field" placeholder="Enter username" type="domain_name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Setting</label>
                                        <select class="form-control" id="time_field">
                                            <option>Set your Time</option>
                                            <option value="* * * * *">Once per minite(* * * * *)</option>
                                            <option value="*/5 * * * *">Once per five minite(*/5 * * * *)</option>
                                            <option value="0,30 * * * *">Twice per hour(0,30 * * * *)</option>
                                            <option value="0 * * * *">Once per hour(0 * * * *)</option>
                                        </select>




                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Set Your Script with time</label>
                                        <input class="form-control" required="required" id="script_field" placeholder="set your script" type="folder_path">
                                    </div>


                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" id="add-cron-submit" class="btn btn-primary">Submit</button>
                                    <span><img class="loading hidden" src="{{ asset('/images/preloader.gif') }}" /> </span>
                                </div>
                            </form>
                        </div>
                        <div><pre class="" id="result"></pre></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Cron script Lists</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Time</th>
                                        <th>Cron script</th>
                                        <th>User id</th>
                                        <th>Created at</th>
                                    </tr>
                                    </thead>
                                    <tbody class="cron_lists_table">
                                    @if(count($cron_lists) != 0)
                                        @foreach($cron_lists as $cron_list)
                                            <tr>
                                                <td class="user_name">{{ $cron_list->user_name }}</td>
                                                <td class="time">{{ $cron_list->time }}</td>
                                                <td class="cron_script">{{ $cron_list->cron_script }}</td>
                                                <td class="user_id">{{ $cron_list->user_id }}</td>
                                                <td class="created_at">{{ $cron_list->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="user_name"></td>
                                            <td class="time"></td>
                                            <td class="cron_script"></td>
                                            <td class="user_id"></td>
                                            <td class="created_at"></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    @include('template.footer')

    <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Home tab content -->
                <div class="tab-pane active" id="control-sidebar-home-tab">
                    <h3 class="control-sidebar-heading">Recent Activity</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript::;">
                                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                    <p>Will be 23 on April 24th</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                    <h3 class="control-sidebar-heading">Tasks Progress</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript::;">
                                <h4 class="control-sidebar-subheading">
                                    Custom Template Design
                                    <span class="pull-right-container">
                  <span class="label label-danger pull-right">70%</span>
                </span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                </div>
                <!-- /.tab-pane -->
                <!-- Stats tab content -->
                <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                <!-- /.tab-pane -->
                <!-- Settings tab content -->
                <div class="tab-pane" id="control-sidebar-settings-tab">
                    <form method="post">
                        <h3 class="control-sidebar-heading">General Settings</h3>

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Report panel usage
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Some information about this general settings option
                            </p>
                        </div>
                        <!-- /.form-group -->
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

@endsection

@section('extra_css_files')
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('extra_js_files')
    <script src="{{ asset('/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
@endsection

@section('extra_js_run')
    <script>

        //            $(function () {
        //                $("#example1").DataTable();
        //            });

        $("#add-cron-submit").click(function(event){
            event.preventDefault();
            $('.loading').removeClass('hidden');
            cronCreation();
        });

        function cronCreation() {
            var user_name_field = $("#user_name_field").val();
            var script_field = $("#script_field").val();
            var time_field = $("#time_field").val();

            $.ajax({
                type: "get",
                url: "/cronjobs/create-cron-job?" + "user_name=" + user_name_field + "&" + "cron_script="+ script_field + "&time=" + time_field ,
                datatype: "html",
                success: function (data) {
                    if(data.result){
                        $('#result').text(data.result);
                        var user_name = data.output.user_name;
                        var time = data.output.time;
                        var cron_script = data.output.cron_script;
                        var user_id = data.output.user_id;
                        var created_at = data.output.created_at;
                        var cron_details_html = $('.cron_lists_table tr:first').clone()
                                .find('.user_name').text(user_name).end()
                                .find('.time').text(time).end()
                                .find('.cron_script').text(cron_script).end()
                                .find('.user_id').text(user_id).end()
                                .find('.created_at').text(created_at).end();
                        $('.cron_lists_table').prepend(cron_details_html);
                    } else {
                        $('#result').text(data.all_log);
                    }
                    $('.loading').addClass('hidden');
                }
            });
        }
    </script>

@endsection







