<?php

Route::group(['middleware' => 'web', 'prefix' => 'domain', 'namespace' => 'Modules\Domain\Http\Controllers'], function()
{
    Route::any('/', [
        'as'   => 'domain.index',
        'uses' => 'DomainController@index'
    ]);

    Route::any('create-domain-account', [
        'as'   => 'ftp.create_account',
        'uses' => 'DomainController@domainCreateshellHit'
    ]);

});
