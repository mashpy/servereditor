<?php

namespace Modules\ManageUsers\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\ManageUsers\Models\CreateUser;

class ManageUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('manageusers::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('manageusers::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store()
    {
      $data= Input::except(array('token'));

       $rule=array(
           'name' => 'required',
           'email' => 'unique:users,email',
           'password' => 'required|min:6',
           'password_confirmation' => 'required|same:password',

       );

        $message =array(
            'password_confirmation.required' => 'please confirm the password',
        'password_confirmation.min' => 'The confirm  password should be 6 characters',
        'password_confirmation.same' => 'Password and confirm password must be same'
        );

        $validator=Validator::make($data,$rule,$message);

        if ($validator->fails()){
           return Redirect::to('manageusers')->withErrors($validator);
        }else{
            CreateUser::formstore(Input::except(array('_token','password_confirmation')));

            return Redirect::to('manageusers')->with('success','User added successfully');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('manageusers::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('manageusers::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
