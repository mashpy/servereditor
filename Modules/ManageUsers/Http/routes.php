<?php

Route::group(['middleware' => 'web', 'prefix' => 'manageusers', 'namespace' => 'Modules\ManageUsers\Http\Controllers'], function()
{
    Route::any('/', [
        'as'   => 'manageusers.index',
        'uses' => 'ManageUsersController@index'
    ]);


    Route::post('/register_action', [
        'as'   => 'manageusers.create',
        'uses' => 'ManageUsersController@store'
    ]);
});
