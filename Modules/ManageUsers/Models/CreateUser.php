<?php
namespace App;
namespace Modules\ManageUsers\Models;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class CreateUser extends Model
{
    protected $table="users";

    protected $fillable = [
        'name', 'email', 'password',
    ];

    public static function formstore($data)
    {
        $username=Input::get('name');
        $email=Input::get('email');
        $pass =Hash::make(Input::get('password'));

        $users=new CreateUser();

        $users->name=$username;
        $users->email=$email;
        $users->password=$pass;
        $users->save();
    }
}