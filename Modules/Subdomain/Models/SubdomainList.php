<?php

namespace Modules\Subdomain\Models;

use Illuminate\Database\Eloquent\Model;

class SubdomainList extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
