<?php

Route::group(['middleware' => 'web', 'prefix' => 'subdomain', 'namespace' => 'Modules\Subdomain\Http\Controllers'], function()
{

    Route::any('/', [
        'as'   => 'subdomain.index',
        'uses' => 'SubdomainController@index'
    ]);

    Route::any('create-subdomain-account', [
        'as'   => 'ftp.create_account',
        'uses' => 'SubdomainController@subdomainCreateshellHit'
    ]);
});
