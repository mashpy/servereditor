<?php

namespace Modules\Subdomain\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Subdomain\Models\SubdomainList;

class SubdomainController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $subdomain_lists = SubdomainList::orderBy('created_at', 'desc')->get();
        return view('subdomain::index')->with(compact('subdomain_lists'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    public function create()
    {
        return view('subdomain::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('subdomain::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('subdomain::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function subdomainCreateshellHit(Request $request){
        $subdomain_name = $request->get('subdomain_name');
        $folder_path = $request->get('folder_path');
        $language_type = $request->get('language_type');

        $subdomain_template = "server {
    listen 80;

    root $folder_path;
    index index.php index.html index.htm;

    server_name $subdomain_name;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location ~ \\.php$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }
}
";
        $execute_array = [["echo '$subdomain_template' > /etc/nginx/sites-available/$subdomain_name.conf", true],
            ["ln -sf /etc/nginx/sites-available/$subdomain_name.conf /etc/nginx/sites-enabled/$subdomain_name.conf", true],
            ["mkdir $folder_path", false],
            ["service nginx restart", true]
        ];

        $add_domain_shell = $this->execute_multiple($execute_array);

        if($add_domain_shell['result']){
            $subdomain_list = new \Modules\Subdomain\Models\SubdomainList;
            $subdomain_list->subdomain_name = $subdomain_name;
            $subdomain_list->folder_path = $folder_path;
            $subdomain_list->language_type = $language_type;
            $subdomain_list->user_id = 1;
            $subdomain_list_save = $subdomain_list->save();
            if($subdomain_list_save){
                $json_output['result'] = true;
                $json_output['output'] = $subdomain_list;
            } else {
                $json_output['result'] = false;
            }
        } else {
            $json_output = $add_domain_shell;
        }

        return $json_output;
    }

    private function  execute_multiple($commands){
        $output = [];
        $output['result'] = true;
        $output['all_log'] = [];
        foreach($commands as $command){
            exec("sudo $command[0] 2>&1", $log, $return_var);
            $output['all_log'] = $log;
            if( $command[1] && $return_var !== 0){
                $output['result'] = false;
                break;
            }
        }

        return $output;
    }
}
