<?php

namespace Modules\MysqlUsers\Models;

use Illuminate\Database\Eloquent\Model;

class MysqlUser extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
