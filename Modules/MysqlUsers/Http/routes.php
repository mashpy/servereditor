<?php

Route::group(['middleware' => 'web', 'prefix' => 'mysql_users', 'namespace' => 'Modules\MysqlUsers\Http\Controllers'], function()
{
    Route::any('/', [
        'as'   => 'mysql_users.index',
        'uses' => 'MysqlUsersController@index'
    ]);

    Route::any('create-mysql-user-account', [
        'as'   => 'mysql_users.create_account',
        'uses' => 'MysqlUsersController@shellMysqlUsersHit'
    ]);
});
