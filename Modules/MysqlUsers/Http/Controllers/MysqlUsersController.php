<?php

namespace Modules\MysqlUsers\Http\Controllers;

use Modules\MysqlUsers\Models\MysqlUser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class MysqlUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $mysql_users = MysqlUser::orderBy('created_at', 'desc')->get();
        return view('mysqlusers::index')->with(compact('mysql_users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('mysqlusers::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('mysqlusers::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('mysqlusers::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function shellMysqlUsersHit(Request $request)
    {
        $db_name = $request->get('db_name');
        $username = $request->get('username');
        $password = $this->randomPassword();
        $json_output['result'] = false;
        $json_output['output'] = '';

        $conn = new \mysqli('localhost', 'root', 'mahmud');

        $sql = "CREATE DATABASE $db_name;
                CREATE USER '$username'@'localhost' IDENTIFIED BY '$password';
                GRANT ALL PRIVILEGES ON $db_name . * TO '$username'@'localhost';
                FLUSH PRIVILEGES;";
        if ($conn->multi_query($sql) === TRUE) {
            $mysql_user = new \Modules\MysqlUsers\Models\MysqlUser;
            $mysql_user->db_name = $db_name;
            $mysql_user->username = $username;
            $mysql_user->password = $password;
            $mysql_user->user_id = 1;
            $mysql_user_save = $mysql_user->save();
            if($mysql_user_save){
                $json_output['result'] = true;
                $json_output['output'] = $mysql_user;
            } else{
                $json_output['result'] = false;
            }

        } else {
            $json_output['result'] = false;
            $json_output['output'] = $conn->error;
        }

        $conn->close();

        return $json_output;
    }

    private function  execute_multiple($commands){
        $output = [];
        $output['result'] = true;
        $output['all_log'] = [];
        foreach($commands as $command){
            exec("sudo $command[0] 2>&1", $log, $return_var);
            $output['all_log'] = $log;
            if( $command[1] && $return_var !== 0){
                $output['result'] = false;
                break;
            }
        }

        return $output;
    }

    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}
