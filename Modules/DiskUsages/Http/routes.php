<?php

Route::group(['middleware' => 'web', 'prefix' => 'disk_usages', 'namespace' => 'Modules\DiskUsages\Http\Controllers'], function()
{
    Route::any('/', [
        'as'   => 'diskusages.index',
        'uses' => 'DiskUsagesController@index'
    ]);

});
