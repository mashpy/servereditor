<?php

namespace Modules\Ftp\Http\Controllers;

use Modules\Ftp\Models\FtpUser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class FtpController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
         $ftp_users = FtpUser::orderBy('created_at', 'desc')->get();
         return view('ftp::index')->with(compact('ftp_users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('ftp::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('ftp::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('ftp::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function shellFtpHit(Request $request){
        $username = $request->get('username');
        $password = $request->get('password');
        $directory = $request->get('folder_dir');
        $resource_dir = '/media/sf_servereditor/app/Console/ServereditorShell/ftp/';
        $json_output['result'] = false;
        $json_output['output'] = '';

        $config_ftp_command = [
            ["apt-get install -y vsftpd libpam-pwdfile apache2-utils" , false],
            ["cp $resource_dir/config/vsftpd.conf /etc/vsftpd.conf" , false],
            ["mkdir -p /etc/vsftpd" , false],
            ["htpasswd -bd /etc/vsftpd/ftpd.passwd $username $password" , false],
            ["cp $resource_dir/config/vsftpd /etc/pam.d/vsftpd" , false],
            ["useradd --home /home/vsftpd --gid nogroup -m --shell /bin/false vsftpd" , false],
            ["mkdir -p /etc/vsftpd_user_conf/" , false]
        ];

        $config_ftp = $this->execute_multiple($config_ftp_command);

        if($config_ftp['result']){
            $path_to_file = "/etc/vsftpd_user_conf/". $username;
            file_put_contents($path_to_file, "local_root=$directory");

            $new_ftp_user_command = [
                ["mkdir -p /etc/vsftpd_user_conf" , true],
                ["mkdir -p $directory" , true],
                ["chown www-data:www-data $directory" , true],
                ["chown www-data:www-data $directory" , true],
                ["chmod 775 $directory" , true],
                ["usermod -a -G www-data vsftpd" , true],
                ["service vsftpd restart" , true]
            ];

            $new_ftp_user = $this->execute_multiple($new_ftp_user_command);

            if($new_ftp_user['result']){
                $ftp_user = new \Modules\Ftp\Models\FtpUser;
                $ftp_user->username = $username;
                $ftp_user->password = $password;
                $ftp_user->folder_path = $directory;
                $ftp_user->user_id = 1;
                $ftp_save = $ftp_user->save();
                if($ftp_save){
                    $json_output['result'] = true;
                    $json_output['output'] = $ftp_user;
                } else {
                    $json_output['result'] = false;
                }
            } else {
                $json_output = $new_ftp_user;
            }
        } else {
            $json_output = $config_ftp;
        }

        return $json_output;
    }

    private function  execute_multiple($commands){
        $output = [];
        $output['result'] = true;
        $output['all_log'] = [];
        foreach($commands as $command){
            exec("sudo $command[0] 2>&1", $log, $return_var);
            $output['all_log'] = $log;
            if( $command[1] && $return_var !== 0){
                $output['result'] = false;
                break;
            }
        }

        return $output;
    }
}
