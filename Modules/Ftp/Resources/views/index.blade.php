@extends('layouts.master')

@section('content')

    <div class="wrapper">
        @include('template.header')
                <!-- Left side column. contains the logo and sidebar -->
        @include('template.main_sidebar')
                <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    FTP Accounts
                    <small>Create FTP account</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                    <li class="active">Here</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Add FTP account</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="" role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Username</label>
                                        <input class="form-control" required="required" id="username" placeholder="Enter username" type="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input class="form-control" required="required" id="password" placeholder="Password" type="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Directory</label>
                                        <input class="form-control" required="required" id="folder_dir" placeholder="Enter directory" type="text">

                                        <p class="help-block">Put directory address carefully.</p>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" id="ftp-submit" class="btn btn-primary">Submit</button>
                                    <span><img class="loading hidden" src="{{ asset('/images/preloader.gif') }}" /> </span>
                                </div>
                            </form>
                        </div>
                        <div><pre class="" id="result"></pre></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">FTP Account Lists</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Password</th>
                                        <th>Folder</th>
                                        <th>Created by</th>
                                        <th>Created at</th>
                                    </tr>
                                    </thead>
                                    <tbody class="ftp_user_table">
                                    @if(count($ftp_users) != 0)
                                        @foreach($ftp_users as $ftp_user)
                                            <tr>
                                                <td class="username">{{ $ftp_user->username }}</td>
                                                <td class="password">{{ $ftp_user->password }}</td>
                                                <td class="folder_path">{{ $ftp_user->folder_path }}</td>
                                                <td class="user_id">{{ $ftp_user->user_id }}</td>
                                                <td class="created_at">{{ $ftp_user->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="username"></td>
                                            <td class="password"></td>
                                            <td class="folder_path"></td>
                                            <td class="user_id"></td>
                                            <td class="created_at"></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        @include('template.footer')

                <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Home tab content -->
                <div class="tab-pane active" id="control-sidebar-home-tab">
                    <h3 class="control-sidebar-heading">Recent Activity</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript::;">
                                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                    <p>Will be 23 on April 24th</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                    <h3 class="control-sidebar-heading">Tasks Progress</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript::;">
                                <h4 class="control-sidebar-subheading">
                                    Custom Template Design
                <span class="pull-right-container">
                  <span class="label label-danger pull-right">70%</span>
                </span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

                </div>
                <!-- /.tab-pane -->
                <!-- Stats tab content -->
                <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                <!-- /.tab-pane -->
                <!-- Settings tab content -->
                <div class="tab-pane" id="control-sidebar-settings-tab">
                    <form method="post">
                        <h3 class="control-sidebar-heading">General Settings</h3>

                        <div class="form-group">
                            <label class="control-sidebar-subheading">
                                Report panel usage
                                <input type="checkbox" class="pull-right" checked>
                            </label>

                            <p>
                                Some information about this general settings option
                            </p>
                        </div>
                        <!-- /.form-group -->
                    </form>
                </div>
                <!-- /.tab-pane -->
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

@endsection

@section('extra_css_files')
    <link rel="stylesheet" href="{{ asset('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('extra_js_files')
    <script src="{{ asset('/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
@endsection

@section('extra_js_run')
    <script>

        //            $(function () {
        //                $("#example1").DataTable();
        //            });

        $("#ftp-submit").click(function(event){
            event.preventDefault();
            $('.loading').removeClass('hidden');
            ftpCreation();
        });

        function ftpCreation() {
            var username_val = $("#username").val();
            var password_val = $("#password").val();
            var folder_dir = $("#folder_dir").val();

            $.ajax({
                type: "get",
                url: "/ftp/create-ftp-account?" + "username=" + username_val + "&" + "password="+ password_val + "&folder_dir=" + folder_dir ,
                datatype: "html",
                success: function (data) {
                    if(data.result){
                        $('#result').text(data.result);
                        var username = data.output.username;
                        var password = data.output.password;
                        var folder_path = data.output.folder_path;
                        var user_id = data.output.user_id;
                        var created_at = data.output.created_at;
                        var ftp_details_html = $('.ftp_user_table tr:first').clone()
                                .find('.username').text(username).end()
                                .find('.password').text(password).end()
                                .find('.folder_path').text(folder_path).end()
                                .find('.user_id').text(user_id).end()
                                .find('.created_at').text(created_at).end();
                        $('.ftp_user_table').prepend(ftp_details_html);
                    } else {
                        $('#result').text(data.all_log);
                    }
                    $('.loading').addClass('hidden');
                }
            });
        }

    </script>

@endsection






