<?php

namespace Modules\FileManager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Settings\Models\Setting;

class FileManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $file_manager_setup = Setting::where('name', 'file_manager_setup')->first();
        return view('filemanager::index', compact('file_manager_setup'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('filemanager::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('filemanager::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('filemanager::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function fileManagerSetup(){
        $config_dir = 'modules/filemanager/';
        $example_file_content = file_get_contents($config_dir.'/config.example.php');
        $new_file_content = str_replace('base_path_value', getcwd().'/'. $config_dir, $example_file_content);
        file_put_contents($config_dir.'/config.php', $new_file_content);
        return redirect('/modules/filemanager/.');
    }
}
