<?php

Route::group(['middleware' => 'web', 'prefix' => 'filemanager', 'namespace' => 'Modules\FileManager\Http\Controllers'], function()
{
    Route::any('/', [
        'as' => 'filemanager.index',
        'uses' => 'FileManagerController@index'
    ]);

    Route::any('file_manager_setup', [
        'as' => 'filemanager.setup',
        'uses' => 'FileManagerController@fileManagerSetup'
    ]);
});
